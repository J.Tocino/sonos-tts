<h2>Sonos TTS - Alternative service</h2>
A PHP class to control Sonos products with a different TTS service. Original class from:
https://github.com/DjMomo/sonos

<h2>What is this used for?</h2>
It is a really basic TTS (Text to Speech) script for Sonos products, you can tweak it to play automated messages and notify the user by audio. 

<h2>What u need!</h2>
A public key from: http://www.voicerss.org/ It is limited to 350 requests a day!

<h2>How-to</h2>
<ol>
    <li>Place all the files on a local webserver</li>
    <li>Create a network share for the audio subdirectory</li>
    <li>Add the network share to your sonos</li>
    <li>Edit the sonos.tts.php with your Sonos IP and Webserver IP</li>
    <li>Place the Voice RSS Api key on the Key Variable</li>
    <li>Run the sonos.tts.php from your local webserver</li>
</ol>

<h2>Other original commands</h2>
<ul>
<li>static get_room_coordinator(string room_name) : Returns an instance of SonosPHPController representing the 'coordinator' of the specified room</li>
<li>static detect(string ip,string port) : IP and port are optional. Returns an array of instances of SonosPHPController, one for each Sonos device found on the network</li>
<li>get_coordinator() : Returns an instance of SonosPHPController representing the 'coordinator' of the room this device is in</li>
<li>device_info() : Gets some info about this device as an array</li>
<li>AddSpotifyToQueue(string spotify_id,bool next) : Adds the provided spotify ID to the queue either next or at the end</li>
<li>Play() : play</li>
<li>Pause() : pause</li>
<li>Stop() : stop</li>
<li>Next() : next track</li>
<li>Previous() : previous track</li>
<li>SeekTime(string) : seek to time xx:xx:xx</li>
<li>ChangeTrack(int) : change to track xx</li>
<li>RestartTrack() : restart actual track</li>
<li>RestartQueue() : restart queue</li>
<li>GetVolume() : get volume level</li>
<li>SetVolume(int) : set volume level</li>
<li>GetMute() : get mute status</li>
<li>SetMute(bool) : active-disable mute</li>
<li>GetTransportInfo() : get status about player</li>
<li>GetMediaInfo() : get informations about media</li>
<li>GetPositionInfo() : get some informations about track</li>
<li>AddURIToQueue(string,bool) : add a track to queue</li>
<li>RemoveTrackFromQueue(int) : remove a track from Queue</li>
<li>RemoveAllTracksFromQueue() : remove all tracks from queue</li>
<li>RefreshShareIndex() : refresh music library</li>
<li>SetQueue(string) : load a track or radio in player</li>
<li>PlayTTS(string message,string station,int volume,string lang) : play a text-to-speech message</li>
</ul>

