<?php
require("sonos.class.php");
$IP_sonos_1 = "YOUR_SONOS_IP_ADDRESS";
$directory = "YOUR_LOCAL_WEBSERVER_IP_ADDRESS"; // No localhost!

$volume = 0;
$force_unmute = 0; 
 
// Voice RSS Key
$key = "YOUR_RSS_KEY_GOES_HERE"; 

// Sonos connection
$sonos_1 = new SonosPHPController($IP_sonos_1, $key);

// Volume to 50%
$sonos_1->setVolume(50);

// Remove Queue
$sonos_1->RemoveAllTracksFromQueue();

// TTS to Mp3 file
$ttsMp3 = $sonos_1->PlayTTS('De nieuwe TTS dienst werkt zonder problemen, probeer een andere tekst!!',$directory,'nl','t'); 

// Place Mp3 file to Queue
$sonos_1->SetQueue($ttsMp3);

// Play it!
$sonos_1->Play();

?>